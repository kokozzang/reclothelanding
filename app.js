var express = require('express');
var routes = require('./routes');
var http = require('http');
var favicon = require('serve-favicon');
var fs = require('fs');
var path = require('path');
var yaml = require('js-yaml');
var expressValidator = require('express-validator');
var expressValidatorConfig = require('./config/expressValidatorConfig');
var mysql = require('mysql');
var AWS = require('aws-sdk');
global. _ = require('lodash');

global.ENUMS = require('./config/enums');
var validator = require('./utils/validator');

var app = express();

var configPath = __dirname + '/config';
global.MALEBOX_CONFIG = yaml.safeLoad(fs.readFileSync(configPath + '/db.yml', 'utf8'));
global.AWS_CONFIG = yaml.safeLoad(fs.readFileSync(configPath + '/aws.yml', 'utf8'));

AWS.config.region = AWS_CONFIG.REGION;
AWS.config.apiVersions = {
  s3: '2006-03-01'
  // other service API versions
};

global.S3 = new AWS.S3(); //S3


var accesslogger = require('./utils/logger').accesslogger;

global.POOL = mysql.createPool({
  host: MALEBOX_CONFIG.MYSQL.HOST,
  port: MALEBOX_CONFIG.MYSQL.PORT,
  user: MALEBOX_CONFIG.MYSQL.USER,
  password: MALEBOX_CONFIG.MYSQL.PASSWORD,
  database: MALEBOX_CONFIG.MYSQL.DB
});

// all environments
app.set('port', process.env.PORT || 3001);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.compress());
var winstonStream = {
  write: function(message, encoding){
    accesslogger.info(message);
  }
};
// default ':remote-addr - - [:date] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"'
// :response-time ms
var opt = {
  stream: winstonStream,
  format: ':remote-addr - - [:date] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" :response-time ms'
};
  app.use(express.logger('dev'));
app.use(express.static(path.join(__dirname, 'public'), {maxAge: 3600000*24*7}));//1주일
//app.use(express.favicon(path.join(__dirname, '/public/img/favicon.ico')));
//app.use(express.static(path.join(__dirname, 'public')));
//app.use(express.logger(opt));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.bodyParser());
app.use(express.session({
  secret: 'linklink',
  key: 'apink',
  cookie: {maxAge: 60*60*1000 * 2}
}));
app.use(expressValidator(expressValidatorConfig));
app.use(express.multipart());       //업로드 가능
app.use(app.router);


app.get('/', routes.index);
//app.get('/register', routes.register);
//app.post('/order', validator.insertOrder, routes.insertOrder);
//app.get('/complete', routes.complete);
//app.get('/logloginin', routes.logloginin);
//app.post('/logloginin', routes.login);
//app.get('/admintest', routes.test);

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}else{
  app.use(function (err, req, res, next) {
    if (~err.message.indexOf('not found')) return next;
    if (req.xhr) {
      err.httpStatus = 500;
//      res.status(err.httpStatus).send({error: err.message});
      console.error(err.message);
      console.error(err);
      res.status(err.httpStatus).send({error: '서버에 문제가 생겼습니다.'});
    } else {
      err.httpStatus = 500;
//      res.render('error', {error: err.message});
      console.error(err.message);
      res.render('error', {error: '서버에 문제가 생겼습니다.'});
    }
  });
}
app.use(function pageNotFound(req, res, next){
  //404
  var error = new Error();
  error.name = 'PageNotFoundError';
//  error.httpStatus = ENUMS.HTTP_STATUS.NOT_FOUND.CODE;
  error.message = ENUMS.HTTP_STATUS.NOT_FOUND.MESSAGE;
  error.url = req.originalUrl;
  res.status(404);
  res.render('error', {error: error.message});
});


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
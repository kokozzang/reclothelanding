$(function () {
  var $lowButton = $('#low_button');
  var screenWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;

  $('#bs-navbar-collapse a').click(function (){
    $('#bs-navbar-collapse').collapse('hide');
//    $('#bs-navbar-collapse').removeClass('in');
  });

  if(!$lowButton.length) {
    if(screenWidth > 767)
      $('#brand .collapse').collapse('show');


    $('li a[href*=#]:not([href=#])').click(function () {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 800);
          return false;
        }
      }
    });

    $('#styling .detail-button').click(function () {
      $('#tryon').addClass('none');
      $('#styling').addClass('none');
      $('#stylingDialog').removeClass('none');
    });
    $('#tryon .detail-button').click(function () {
      $('#tryon').addClass('none');
      $('#styling').addClass('none');
      $('#tryonDialog').removeClass('none');

    });

    $('.close-button').click(function () {
      $(this).parent().parent().parent().parent().addClass('none');
      $('#tryon').removeClass('none');
      $('#styling').removeClass('none');
    });


  } else{
    imagesLoaded('#level', function (){
    //윈도우 리사이즈시 스타일이미지 체크 백그라운드 이미지 크키도 리사이즈함
      var width = $('.styleCheck .img-responsive').width();
      $('.styleCheck .checked').width(width);
      $('.styleCheck .checked').height(width);
    });

    imagesLoaded('#styles', function (){
      var $levelImage = $('.levelcard .img-responsive');
      var width = $levelImage.width();
      $('.levelcard .checked').width(width);
      $('.levelcard .checked').height(width);
      $levelImage.each(function(){
        $(this).next('.checked').css('top', $(this).position().top);
        $(this).next('.checked').css('left', $(this).position().left);
      });
    });


    $("input[name='style']").click(function(){
      if($(this).val() !== '0')
        $("#styleId0").prop('checked', false);
    });
    $("#styleId0").click(function (){
      $("input[name='style']").prop('checked', false);
      $(this).prop('checked', true);
    });

    $("input[name='hate']").click(function(){
      if($(this).val() !== '0')
        $("#hateId0").prop('checked', false);
    });
    $("#hateId0").click(function (){
      $("input[name='hate']").prop('checked', false);
      $(this).prop('checked', true);
    });


    $(window).on('resize', function(){
      imagesLoaded('#level', function () {
        var width = $('.styleCheck .img-responsive').width();
        $('.styleCheck .checked').width(width);
        $('.styleCheck .checked').height(width);
      });

      imagesLoaded('#styles', function () {
        var $levelImage = $('.levelcard .img-responsive');
        var width = $levelImage.width();
        $('.levelcard .checked').width(width);
        $('.levelcard .checked').height(width);
        $levelImage.each(function () {
          $(this).next('.checked').css('top', $(this).position().top);
          $(this).next('.checked').css('left', $(this).position().left);
        });
      });
    });


    $('.levelcard img').click(function(){
      $(this).siblings('.caption').children('input[type=radio]').click();
    });


    $('input:radio[name=level]').click(function(){
      $('input:radio[name=level]').each(function(){
        $(this).prop('checked', false);
        $(this).parent().prev('.checked').css('z-index', -1);
      });
      $(this).prop('checked', true);
      $(this).parent().prev('.checked').css('z-index', 2);
    });



    $.validator.addMethod("korOnly", function(value) {
      var reg = /^[가-힣]+$/i;
      if(!reg.test(value))
        return false;
      return true;
    }, $.validator.format("한글만 입력 가능합니다."));

    $.validator.addMethod("phoneNumber", function(value) {
      var regex = /^\(?([0-9]{3})\)?([ .-]?)([0-9]{3,4})\2([0-9]{4})$/;
      if(regex.test(value))
        return true;
      return false;
    }, $.validator.format("휴대폰 번호를 확인해주세요."));


    $('#birth').datepicker({
      language: 'kr',
      format: 'yyyy-mm-dd',
      autoclose: true,
      startView: 'decade'
    }).on('changeDate', function(e){
//      $('#registerInfoForm').bootstrapValidator('revalidateField', 'birth');
    });


    $('#postcode').focus(function(){
      $('#postcodeButton').click();
      $(this).blur();
    });
    $('#address1').focus(function(){
      $('#postcodeButton').click();
      $(this).blur();
    });


    $("input:checkbox.agree").each(function() {
      $(this).click(function(){
        if(!$(this).hasClass('agreeAll')){
          var checked = $("input:checkbox.agree:checked").map(function(){
            return $(this).val();
          }).get();

          if($.inArray('0', checked) === -1 && $.inArray('1', checked) > -1 && $.inArray('2', checked) > -1)
            $('#agreeAll').prop('checked', true);
          else
            $('#agreeAll').prop('checked', false);

          if(!$(this).prop('checked')) {
            $('#agreeAll').prop('checked', false);
          }
        }else{
          if($('#agreeAll').prop('checked')){
            $("input:checkbox.agree").each(function() {
              $(this).prop('checked', true);
            });
          }else{
            $("input:checkbox.agree").each(function() {
              $(this).prop('checked', false);
            });
          }
        }
      });
    });



//    $("input[name=agree]:checkbox").each(function() {
//      $(this).click(function(){
//        if(!$(this).hasClass('agreeAll')){
//          var checked = $("input[name=agree]:checkbox:checked").map(function(){
//            return $(this).val();
//          }).get();
//
//          if($.inArray('0', checked) === -1 && $.inArray('1', checked) > -1 && $.inArray('2', checked) > -1)
//            $('#agreeAll').prop('checked', true);
//          else
//            $('#agreeAll').prop('checked', false);
//
//          if(!$(this).prop('checked')) {
//            $('#agreeAll').prop('checked', false);
//          }
//        }else{
//          if($('#agreeAll').prop('checked')){
//            $("input[name=agree]:checkbox").each(function() {
//              $(this).prop('checked', true);
//            });
//          }else{
//            $("input[name=agree]:checkbox").each(function() {
//              $(this).prop('checked', false);
//            });
//          }
//        }
//      });
//    });

    var test = 0;
    $('#orderForm').validate({
//      debug: true,

      submitHandler: function(form) {
        if(test == 1){
          alert('잠시만 기다려주세요');
          return false;
        }
        test = 1;
        $.ajax({
          url: '/order',
          type: 'POST',
          data: new FormData(form),
          mimeType: 'multipart/form-data',
          dataType: 'json',
          contentType: false,
          cache: false,
          processData:false,
          statusCode: {
            200: function() {
              location.replace('/complete');
            },
            404: function() {
              test = 0;
              alert('유효하지 않은 요청입니다.');
            },
            500: function() {
              test = 0;
              alert('서버에 문제가 생겼습니다ㅠㅠ 잠시 후에 다시 신청해주세요');
            }
          },
//          error: function(jqXHR, textStatus, errorThrown){
//            console.log(errorThrown);
//            console.log(textStatus);
//            console.log(jqXHR);
//          },
          beforeSend:function(){
            $('.loading').removeClass('none');

          },
          complete:function(){
            $('.loading').addClass('none');
          }
        });

      },
      ignore: [],
      invalidHandler: function(form, validator) {
        if (!validator.numberOfInvalids())
          return;

        var $scrollTo = $(validator.errorList[0].element);

        if($scrollTo.attr('type') === 'radio'
          ||$scrollTo.attr('type') === 'checkbox' )
          $scrollTo = $scrollTo.parent();

        var scrollh = (screenWidth > 767)? 500: 350;
        $('html, body').animate({
          scrollTop: $scrollTo.offset().top - scrollh
        }, 1000);
        $scrollTo.focus();
      },
      errorPlacement: function(label, element) {
        var $labelled = null;
        if($(element).attr('type') === 'radio' || $(element).attr('type') === 'checkbox')
          $labelled = $(element).closest('.pp').find('.lp');
        else
          $labelled = $(element).parent();

        if($labelled.hasClass('has-success')){  //성공 class가 있을때
          $labelled.removeClass('has-success');  //성공 class 제거
        }
        $labelled.addClass('has-error').append(label);
      },
      //validation에 맞는 input 태그에 클래스 추가
      success: function(label, element) {
        var $labelled = null;
        if($(element).attr('type') === 'radio' || $(element).attr('type') === 'checkbox')
          $labelled = $(element).closest('.pp').find('.lp');
        else
          $labelled = $(element).parent();

        if($labelled.hasClass('has-error')){  //실패 class가 있을때
          $labelled
            .removeClass('has-error')//실패 class 제거
            .children('label.error').remove();  //실패 label 제거
        }
        $labelled.addClass('has-success');
      },
      rules: {
        /*레벨*/
        level: {
          required: true,
          digits: true,
          range: [0, 2]
        },
        /*스타일*/
        style: {
          required: true,
          digits: true,
          range: [0, 7]
        },
        hate: {
          required: true,
          digits: true,
          range: [0, 7]
        },
        /*신체 사이즈*/
        height: {
          required: true,
          digits: true,
          range: [165, 190]
        },
        weight: {
          required: true,
          digits: true,
          range: [50, 100]
        },
        top_size: {
          required: true,
          digits: true,
          range: [90, 110]
        },
        bottom_size: {
          required: true,
          digits: true,
          range: [27, 36]
        },
        /*개인정보*/
        name: {
          required: true,
          korOnly: true
        },
        phone: {
          required: true,
          phoneNumber: true
        },
        email: {
          required: true,
          email:true
        },
        postcode: {
          required: true
        },
        address1: {
          required: true
        },
        address2: {
          required: true,
          maxlength: 40
        },
        birth: {
          required: true,
          date: true
        },
        survey: {
          required: true,
          digits: true,
          range: [0, 5]
        },
        consult: {
          required: true,
          digits: true,
          range: [10, 19]
        },
        self: {
          required: true,
          extension: 'png|jpg|jpeg|bmp|gif'
        },
        photo: {
//          required: true,
          extension: 'png|jpg|jpeg|bmp|gif'
        },
        product: {
          required: true,
          maxlength: 4
        },
        termsAgree: {
          required: true,
          range: [1, 1]
        },
        personalAgree: {
          required: true,
          range: [2, 2]
        }
      },
      messages: {
        /*레벨*/
        level: {
          required: '필수 항목입니다.',
          digits: '범위에서 벗어났습니다.',
          range: '범위에서 벗어났습니다.'
        },
        style: {
          required: '필수 항목입니다.',
          digits: '범위에서 벗어났습니다.',
          range: '범위에서 벗어났습니다.'
        },
        hate: {
          required: '필수 항목입니다.',
          digits: '범위에서 벗어났습니다.',
          range: '범위에서 벗어났습니다.'
        },
        /*신체 사이즈*/
        height: {
          required: '필수 항목입니다.',
          digits: '범위에서 벗어났습니다.',
          range: '범위에서 벗어났습니다.'
        },
        weight: {
          required: '필수 항목입니다.',
          digits: '범위에서 벗어났습니다.',
          range: '범위에서 벗어났습니다.'
        },
        top_size: {
          required: '필수 항목입니다.',
          digits: '범위에서 벗어났습니다.'
        },
        bottom_size: {
          required: '필수 항목입니다.',
          digits: '범위에서 벗어났습니다.',
          range: '범위에서 벗어났습니다.'
        },
        /*개인정보*/
        name: {
          required: '필수 항목입니다.',
          korOnly: '한글만 입력 가능합니다. ex)가,나,다,...'
        },
        phone: {
          required: '필수 항목입니다.',
          phoneNumber: '휴대폰 번호를 확인해주세요. ex)000-0000-0000, 000.0000.0000'
        },
        email: {
          required: '필수 항목입니다.',
          email: '이메일 형식에 맞게 입력해주세요.'
        },
        postcode: {
          required: '우편번호를 검색해주세요.'
        },
        address1: {
          required: '우편번호를 검색해주세요.'
        },
        address2: {
          required: '필수 항목입니다.',
          maxlength: '40자 이내로 작성해주세요.'
        },
        birth: {
          required: '필수 항목입니다.',
          date: '날짜 형식으로 입력해주세요.'
        },
        survey: {
          required: '필수 항목입니다.',
          digits: '범위에서 벗어났습니다.',
          range: '범위에서 벗어났습니다.'
        },
        consult: {
          required: '필수 항목입니다.',
          digits: '범위에서 벗어났습니다.',
          range: '범위에서 벗어났습니다.'
        },
        self: {
          required: '필수 항목입니다.',
          extension: 'jpg, jpeg, png, bmp, gif 파일만 가능합니다.'
        },
        photo: {
          required: '필수 항목입니다.',
          extension: 'jpg, jpeg, png, bmp, gif 파일만 가능합니다.'
        },
        product: {
          required: '필수 항목입니다.',
          maxlength: '최대 4개만 선택 가능합니다.'
        },
        termsAgree: {
          required: '필수 항목입니다.',
          range: '범위에서 벗어났습니다.'
        },
        personalAgree: {
          required: '필수 항목입니다.',
          range: '범위에서 벗어났습니다.'
        }
      }
    });

  }

});
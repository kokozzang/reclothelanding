var _ = require('lodash');
var crypto = require('crypto');
var gm = require('gm');
var fs = require('fs');
var path = require('path');
var url = require('url');
var async = require('async');
var _ = require('lodash');

exports.index = function(req, res){
  res.status(200);
  res.sendfile('public/html/index.html');
  //res.sendfile('public/html/comming.html');
};

exports.logloginin = function (req, res){
  res.sendfile('public/html/logloginin.html');
}

exports.login = function (req, res){
  //if(req.body.pwd == 're!clothe') {
  //  req.session.apink = 'bomi';
  //  res.redirect('/admintest');
  //}else{
    res.send(401);
  //}
}

exports.complete = function(req, res, handlerError){
  res.status(200);
  res.sendfile('public/html/complete.html');
};

exports.register = function(req, res){
  res.status(200);
  res.render('register', {
    styles: ENUMS.STYLE_IMAGES,
    hates: ENUMS.HATE_IMAGES
  });
};


exports.test = function (req, res, handlerError){
  if(req.session.apink !== 'bomi')
    return res.send(401);
  req.session.touch();

  var results = {};
  POOL.getConnection(function (err, connection) {
    if (err) { return handlerError(err, null); }
    async.waterfall([
      function (cb){
        var stmt = 'SELECT ' +
          'u.id AS user_id, u.email, u.name, u.phone, u.birth, u.survey, u.consult, u.self, u.photo, ' +
          'u.postcode, u.address1, u.address2, ' +
          'r.id AS request_id, r.message, r.cdate, ' +
          'u.level, bs.height, bs.weight, bs.top, bs.bottom ' +
          'FROM user AS u ' +
          'INNER JOIN request AS r ON u.id = r.user_id ' +
          'INNER JOIN bodysize AS bs ON u.id = bs.user_id';

        connection.query(stmt, function(err, result){
          if(err)
            return cb(err, null);
          results.result = result;
          return cb(null, null);
        });
      },
      function (rs, cb){
        var stmt = 'SELECT ' +
          'u.id AS user_id, us.style ' +
          'FROM user AS u ' +
          'INNER JOIN userstyle AS us ON u.id = us.user_id';

        connection.query(stmt, function(err, result){
          if(err)
            return cb(err, null);

          var userIds = _.uniq(_.pluck(result, 'user_id')); //userIds
          for(var i = 0; i < userIds.length; i++){
            //유저 아이디당 선택한 스타일
            var style = _.pluck(_.where(result, {user_id: userIds[i]}), 'style');
            for(var j = 0; j < results.result.length; j++)
              if(results.result[j].user_id == userIds[i])
                results.result[j].userStyle = style; //해당 유저아이디에 맞게 스타일 넣기
          }

          return cb(null, null);
        });
      },
      function (rs, cb){
        var stmt = 'SELECT ' +
          'u.id AS user_id, uh.hate ' +
          'FROM user AS u ' +
          'INNER JOIN userhate AS uh ON u.id = uh.user_id';

        connection.query(stmt, function(err, result){
          if(err)
            return cb(err, null);

          var userIds = _.uniq(_.pluck(result, 'user_id')); //userIds
          for(var i = 0; i < userIds.length; i++){
            //유저 아이디당 선택한 스타일
            var hate = _.pluck(_.where(result, {user_id: userIds[i]}), 'hate');
            for(var j = 0; j < results.result.length; j++)
              if(results.result[j].user_id == userIds[i])
                results.result[j].userHate = hate; //해당 유저아이디에 맞게 스타일 넣기
          }
          return cb(null, null);
        });
      },
      function (rs, cb){
        var stmt = 'SELECT ' +
          'r.id AS request_id, r.user_id, p.product ' +
          'FROM request AS r ' +
          'INNER JOIN product AS p ON r.id = p.request_id';

        connection.query(stmt, function(err, result){
          if(err)
            return cb(err, null);
          var requestIds = _.uniq(_.pluck(result, 'request_id')); //주문번호들
          for(var i = 0; i < requestIds.length; i++){
            //주문번호당 주문한 상품들
            var product = _.pluck(_.where(result, {request_id: requestIds[i]}), 'product');
            for(var j = 0; j < results.result.length; j++)
              if(results.result[j].request_id == requestIds[i])
                results.result[j].products = product; //해당 주문번호에 맞게 주문상품 리스트 넣기
          }
          return cb(null, null);
        });
      }
    ], function (err, result){
      connection.release();
      if(err)
        return handlerError(err, null);

      res.status(200);
      return res.render('test', results)
    });
  });
};


exports.insertOrder = function (req, res, handlerError){
  //개인정보
  var user = {
    name: req.body.name,
    email: req.body.email,
    phone: req.body.phone,
    birth: req.body.birth,
    level: req.body.level,
    survey: req.body.survey,
    consult: req.body.consult,
    postcode: req.body.postcode,
    address1: req.body.address1,
    address2: req.body.address2,
    self: null,
    photo: null,
    terms: req.body.termsAgree = 1,
    personal: req.body.personalAgree = 1
  };

  //신체 사이즈
  var bodySize = {
    user_id: null,
    height: req.body.height,
    weight: req.body.weight,
    top: req.body.top_size,
    bottom: req.body.bottom_size
  };

  //상품군
  var request = {
    user_id: null,
    message: req.body.message,
    cdate: new Date()
  };

  var styles = req.body.style;
  var hates = req.body.hate;


  var product = req.body.product;

  POOL.getConnection(function (err, connection) {
    if (err) { return handlerError(err, null); }
    var userId = null;
    var requestId = null;
    async.waterfall([
      function transactionStart(cb){
        connection.beginTransaction(function (err){
          if(err)
            return cb(err, null);
          return cb(null, null);
        });
      },
      function imageUpload(result, cb){
        s3Uploader(req.files.self, function (err, url){
          if(err)
            return cb(err, null);
          user.self = url;
          return cb(null, null)
        });
      },
      function imageUpload(result, cb){
        if(req.files.photo === null)
          return cb(null, null);
        s3Uploader(req.files.photo, function (err, url){
          if(err)
            return cb(err, null);
          user.photo = url;
          return cb(null, null)
        });
      },
      //개인 정보
      function (result, cb) {
        var stmt = 'INSERT INTO user SET ?';
        connection.query(stmt, user, function (err, result){
          if(err)
            return cb(err, null);
          userId = result.insertId;
          return cb(null, null);
        });
      },
      //신체 사이즈
      function (result, cb) {
        bodySize.user_id = userId;
        var stmt = 'INSERT INTO bodysize SET ?';
        connection.query(stmt, bodySize, function (err, result){
          if(err)
            return cb(err, null);
          return cb(null, null);
        });
      },
      //선호 스타일
      function (result, cb) {
        var userStyles = [];
        for(var i = 0; i < styles.length; i++) {
          userStyles.push([userId, styles[i]]);
        }

        var stmt = 'INSERT INTO userstyle(user_id, style) VALUES ?';
        connection.query(stmt, [userStyles], function (err, result){
          if(err)
            return cb(err, null);
          return cb(null, null);
        });
      },
      //비선호 스타일
      function (result, cb) {
        var userHates = [];
        for(var i = 0; i < hates.length; i++) {
          userHates.push([userId, hates[i]]);
        }

        var stmt = 'INSERT INTO userhate(user_id, hate) VALUES ?';
        connection.query(stmt, [userHates], function (err, result){
          if(err)
            return cb(err, null);
          return cb(null, null);
        });
      },
      //주문
      function (result, cb) {
        request.user_id = userId;
        var stmt = 'INSERT INTO request SET ?';
        connection.query(stmt, request, function (err, result){
          if(err)
            return cb(err, null);
          requestId = result.insertId;
          return cb(null, null);
        });
      },
      //상품
      function (result, cb) {
        var products = [];
        for(var i = 0; i < product.length; i++) {
          products.push([requestId, product[i]]);
        }

        var stmt = 'INSERT INTO product(request_id, product) VALUES ?';
        connection.query(stmt, [products], function (err, result){
          if(err)
            return cb(err, null);
          return cb(null, null);
        });
      }
    ],
    function (err, results){
      if(err) {
        connection.rollback();
        connection.release();
        return handlerError(err, null);
      }
      connection.commit(function(err) {
        if (err) {
          connection.rollback();
          connection.release();
          return handlerError(err, null);
        }
        connection.release();
        return res.send(200);
      });//end of commit
    });//end of waterfall
  });//end of getConnecton
};

function s3Uploader (image, callback) {
  var originalFileName = image.originalFilename;

  //파일명 해시
  var sha1 = crypto.createHash('sha1');                    // sha1 해쉬 객체 생성
  var currentDate = (new Date()).valueOf().toString();   // 오늘 날짜 타임스탬프
  var randomNum = Math.random().toString();              // 랜덤 숫자 생성
  var hashedName = sha1.update(originalFileName + currentDate + randomNum)
    .digest('hex') + '.jpg'; //업로드할 파일 이름 생성(해쉬)

  //사이즈별 디렉토리에 해쉬파일명으로 디렉토리 생성
  var folder = hashedName.substring(0,2) + '/'
    + hashedName.substring(2,4);
//    + '/'
//    + hashedName.substring(4,6);
  var bucket = AWS_CONFIG.S3.BUCKET + '/' + folder;

  var readStream = fs.createReadStream(image.path); //리드 스트림


  var size = null;
  async.waterfall([
    function calSize(cb) {
      gm(readStream).size(function (err, result){
        if(err)
          return cb(err, null);
        size = result;
        return cb(null, null);
      });
    },
    function createBucket(result, cb) {
      S3.createBucket({Bucket: bucket + '/'}, function(err, data) {
        if(err)
          return cb(err, null);
        return cb(null, null)
      });
    },
    function resizeAndUpload(result, cb) {
      readStream = fs.createReadStream(image.path); //리드 스트림
      var stat = fs.statSync(image.path);

      if(stat.size < 102400 || size.width < 1000) {  //100kb
        gm(readStream)
          .quality(100)
          .stream('jpg', function (err, stdout, stderr) {
            if (err)
              return cb(err, null);

            var buf = new Buffer(0);
            stdout.on('data', function (data) {
              buf = Buffer.concat([buf, data]);
            });
            var expires = new Date();
            expires.setDate(expires.getDate() + 1);
            stdout.on('end', function () {
              var params = {
                Bucket: bucket,
                Key: hashedName,
                Body: buf,
                ContentType: 'image/jpeg',
                ACL: 'private'
                //              ACL: 'public-read'
              };
              S3.putObject(params, function (err, data) {
                if (err)
                  return cb(err, null);
                return cb(null, null);
              });
            });
          });

      }else{
        var ratio = size.height / size.width;
        size.width = 1000;
        size.height = Math.round(size.width * ratio);

        gm(readStream)
          .resize(size.width, size.height)
          .quality(100)
          .stream('jpg', function (err, stdout, stderr) {
            if (err)
              return cb(err, null);

            var buf = new Buffer(0);
            stdout.on('data', function (data) {
              buf = Buffer.concat([buf, data]);
            });
            var expires = new Date();
            expires.setDate(expires.getDate() + 1);
            stdout.on('end', function () {
              var params = {
                Bucket: bucket,
                Key: hashedName,
                Expires: expires,
                //              Expires: 86400,
                Body: buf,
                ContentType: 'image/jpeg',
                ACL: 'private'
                //              ACL: 'public-read'
              };
              S3.putObject(params, function (err, data) {
                if (err)
                  return cb(err, null);
                return cb(null, null);
              });
            });
          });

      }
    }
  ], function (err, results){
    if (err)
      return callback(err, null);
    return callback(null, AWS_CONFIG.S3.URL + folder + '/' + hashedName);
  });
}
var _ = require('lodash');
var sanitizer = require('./sanitizer');
var util = require('util');
//validation 메소드

module.exports.insertOrder = insertOrder; //주문 하기


var MSG_VALIDATION = ENUMS.MSG_VALIDATION;


function insertOrder (req, res, next){
  /*레벨*/
  req.checkBody('level', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('level').toInt();
  req.checkBody('level', MSG_VALIDATION.IS_RANGED).isIn(_.values(ENUMS.LEVEL));


  var styleImageCodes = [];
  for(var i = 0; i < ENUMS.STYLE_IMAGES.length; i++)
    styleImageCodes.push(ENUMS.STYLE_IMAGES[i].CODE);


  /*스타일*/
  req.checkBody('style', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.checkBody('style', MSG_VALIDATION.IS_ARRAY).isArrayOrInteger();
  req.checkBody('style', MSG_VALIDATION.IS_ARRAY_LENGTH).isArrayLength(1, ENUMS.STYLE_IMAGES.length);
  req.body.style = sanitizer.toIntArray(req.body.style);  //integer array로 변환
  req.checkBody('style', MSG_VALIDATION.IS_ARRAY_CONTAINS).isArrayContains(styleImageCodes);


  var hateImageCodes = [];
  for(var i = 0; i < ENUMS.HATE_IMAGES.length; i++)
    hateImageCodes.push(ENUMS.HATE_IMAGES[i].CODE);

  /*싫어하는 스타일*/
  req.checkBody('hate', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.checkBody('hate', MSG_VALIDATION.IS_ARRAY).isArrayOrInteger();
  req.checkBody('hate', MSG_VALIDATION.IS_ARRAY_LENGTH).isArrayLength(1, ENUMS.HATE_IMAGES.length);
  req.body.hate = sanitizer.toIntArray(req.body.hate);  //integer array로 변환
  req.checkBody('hate', MSG_VALIDATION.IS_ARRAY_CONTAINS).isArrayContains(hateImageCodes);

  /*신체 사이즈*/
//  키
  req.checkBody('height', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('height').toInt();
  req.checkBody('height', MSG_VALIDATION.IS_RANGED).isRanged(ENUMS.HEIGHT);
  //몸무게
  req.checkBody('weight', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('weight').toInt();
  req.checkBody('weight', MSG_VALIDATION.IS_RANGED).isRanged(ENUMS.WEIGHT);
  //상의
  req.checkBody('top_size', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('top_size').toInt();
  req.checkBody('top_size', MSG_VALIDATION.IS_IN).isIn(_.values(ENUMS.TOP_SIZE));
  //하의
  req.checkBody('bottom_size', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('bottom_size').toInt();
  req.checkBody('bottom_size', MSG_VALIDATION.IS_RANGED).isRanged(ENUMS.BOTTOM_SIZE);


  /*개인정보*/
  //이름
  req.checkBody('name', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('name').toString();
  req.sanitize('name').trim();
  req.checkBody('name', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.NAME);
  req.sanitize('name').escape();
  //이메일
  req.checkBody('email', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('email').toString();
  req.sanitize('email').trim();
  req.checkBody('email', MSG_VALIDATION.IS_EMAIL).isEmail();
  req.checkBody('email', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.EMAIL);
  req.sanitize('email').normalizeEmail();
  req.sanitize('email').escape();
  //폰
  req.checkBody('phone', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('phone').toString();
  req.sanitize('phone').trim();
  req.checkBody('phone', MSG_VALIDATION.IS_PHONE_NUMBER).isPhoneNumber();
  req.body.phone = req.body.phone.replace(/[^0-9]?/g,'');  //숫자만 추출
  req.checkBody('phone', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.PHONE);
  //우편번호
  req.checkBody('postcode', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('postcode').toString();
  req.checkBody('postcode', MSG_VALIDATION.IS_POST_CODE).isPostCode();
  //주소1
  req.checkBody('address1', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('address1').toString();
  req.sanitize('address1').trim();
  req.checkBody('address1', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.ADDRESS);
  req.sanitize('address1').escape();
  //주소2
  req.checkBody('address2', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('address2').toString();
  req.sanitize('address2').trim();
  req.checkBody('address2', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.ADDRESS);
  req.sanitize('address2').escape();
  //생일
  req.checkBody('birth', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('birth').toDate();
  req.checkBody('birth', MSG_VALIDATION.IS_DATE).isDate();
  //설문
  req.checkBody('survey', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('survey').toInt();
  req.checkBody('survey', MSG_VALIDATION.IS_IN).isIn(_.values(ENUMS.SURVEY));
  //연락 가능한 시간
  req.checkBody('consult', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('consult').toInt();
  req.checkBody('consult', MSG_VALIDATION.IS_RANGED).isRanged(ENUMS.CONSULT_HOUR);
  //사진
  req.checkFiles('self', MSG_VALIDATION.IS_IMAGE).isImage();
  if(req.files.photo.originalFilename)
    req.checkFiles('photo', MSG_VALIDATION.IS_IMAGE).isImage();
  else
    req.files.photo = null;

  /*상품군*/
  var productCodes = [];
  _.forIn(ENUMS.PRODUCT, function(sort, k){
    _.forIn(sort, function(clothes, key){
      productCodes.push(clothes.CODE);
    });
  });

  //상품 신청
  req.checkBody('product', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.checkBody('product', MSG_VALIDATION.IS_ARRAY).isArrayOrInteger();
  req.checkBody('product', MSG_VALIDATION.IS_ARRAY_LENGTH).isArrayLength(1, ENUMS.LIMIT.PRODUCT);
  req.body.product = sanitizer.toIntArray(req.body.product);  //integer array로 변환
  req.checkBody('product', MSG_VALIDATION.IS_ARRAY_CONTAINS).isArrayContains(productCodes);
  //메시지
  req.sanitize('message').toString();
  req.sanitize('message').trim();
  req.sanitize('message').escape();

  /*동의*/
  req.checkBody('termsAgree', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('termsAgree').toInt();
  req.checkBody('termsAgree', MSG_VALIDATION.EQUALS).equals(ENUMS.AGREE.TERMS);

  req.checkBody('personalAgree', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('personalAgree').toInt();
  req.checkBody('personalAgree', MSG_VALIDATION.EQUALS).equals(ENUMS.AGREE.PERSONAL);


  var errors = req.validationErrors();
  if(errors) {
    console.log(util.inspect(errors,{ showHidden: true, depth: 4} ));
    return next(errors);
  }
  return next();
}
var winston = require('winston');
require('winston-mongodb').MongoDB;

var console = {
//    level: 'silly',
  colorize: 'true'
//    label: 'category one'
};
var accesslogger = {
  collection: 'accesslogs',
  dbUri: 'mongodb://'
    + MALEBOX_CONFIG.MONGODB.USER + ':'+ MALEBOX_CONFIG.MONGODB.PASSWORD
    + '@' + MALEBOX_CONFIG.MONGODB.HOST
    + ':' + MALEBOX_CONFIG.MONGODB.PORT  + '/' + MALEBOX_CONFIG.MONGODB.DB
};

winston.loggers.add('accesslog', {
  console: console,
  MongoDB: accesslogger
});

module.exports.accesslogger = winston.loggers.get('accesslog');
var validator = require('validator');

module.exports.toIntArray = toIntArray;

//정수 배열 만들기
function toIntArray(arr){
  if(_.isArray(arr)) {
    for (var i = 0; i < arr.length; i++)
      arr[i] = parseInt(arr[i]);
  }else {
    var temp = parseInt(arr);
    arr = [];
    arr.push(temp);
  }
  return arr;
}
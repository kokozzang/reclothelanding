var _ = require('lodash');
var validator = require('validator');
module.exports = {
  customValidators: {
    isPhoneNumber: function (value){
      var regex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3,4})\2?([ .-]?)([0-9]{4})/;
      if(regex.test(value))
        return true;
      return false;
    },

    isPostCode: function (value){
      var regex = /\(?([0-9]{3})\)?([-]?)([0-9]{3})/;
      if(regex.test(value))
        return true;
      return false;
    },
    //범위 검사
    isRanged: function (value, range){
      if(value >= range.MIN && value <= range.MAX)
        return true;
      return false;
    },
    isArray: function (arr){
      if(_.isArray(arr))
        return true;
      return false;
    },
    isArrayOrInteger: function (value) {
      if(_.isArray(value))
        return true;
      else if(validator.isInt(value))
        return true;
      else
        return false;
    },
    //배열 길이 검사
    isArrayLength: function (arr, min, max){
      if(_.isArray(arr)) {
        var length = arr.length;
        if (length >= min && length <= max)
          return true;
      }else if(validator.isInt(arr)){
        if(min > 1)
          return false;
        return true;
      }else
        return false;
    },
    //배열의 부분집합인지 검사하여 아닌 경우 false 맞으면 true
    isArrayContains: function (arr, container){
      console.log(arr, container);
      if(_.difference(arr, container).length === 0)
        return true;
      return false;
    },
    //한글, 영어, 숫자 검사
    isAlphaKorNumeric: function(value) {
      var reg = /^[가-힣a-zA-Z0-9]+$/i;
      if(!reg.test(value))
        return false;
      return true;
    },
    //양의 정수 검사
    isPositiveNumber: function(value) {
      if(!validator.isInt(value))
        return false;
      if(parseInt(value) < 1)
        return false;
      return true;
    },
    //업로드 이미지 검사
    isImage: function(file) {
      if(file === undefined){
        console.error('파일이 없음.');
        return false;
      }
      var IMAGE_CONTENT_TYPE = {
        jpeg: 'image/jpeg',
        jpg: 'image/jpeg',
        png: 'image/png',
        bmp: 'image/bmp'
      };
      //파일 확장자 추출
      var ext = (/[.]/.exec(file.originalFilename))
        ? /[^.]+$/.exec(file.originalFilename)  //있으면 배열로 반환 ext[0]이 확장자
        : undefined;  //확장자가 없으면 undefined
      if(!validator.isIn(file.type, _.values(IMAGE_CONTENT_TYPE))){  //파일 타입 검사
        console.error('허용되지 않은 파일 타입. ' + 'file.type: ' + file.type);
        return false;
      }if(ext === undefined){ // 파일 확장자가 있는지 검사
        console.error('파일 확장자가 없음. ' + 'file.originalfileName: ' + file.originalFilename);
        return false;
      }if(!validator.isIn(ext[0].toLowerCase(), _.keys(IMAGE_CONTENT_TYPE))){ //확장자검사
        console.log('허용되지 않은 파일 확장자. ' + 'ext: ' + ext[0]);
        return false;
      }
      return true;
    }
  }
};
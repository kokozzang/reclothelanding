module.exports = (function () {
  var enums = {};
  var reverseKeyAndValue = function (obj) {
    var keys = Object.keys(obj);
    var newObj = {};
    keys.forEach(function (key) {
      newObj[obj[key]] = key;
    });
    return newObj;
  };

  enums.LEVEL = {
    LOW: 0,
    MEDIUM: 1,
    HIGH: 2
  };

  enums.STYLE_IMAGES = [
    {
      CODE: 0,
      URL: '/img/nothing.jpg'
    },
    {
      CODE: 1,
      URL: '/img/style/casual1.jpg'
    },
    {
      CODE: 2,
      URL: '/img/style/casual2.jpg'
    },
    {
      CODE: 3,
      URL: '/img/style/unique.jpg'
    },
    {
      CODE: 4,
      URL: '/img/style/semisuit.jpg'
    },
    {
      CODE: 5,
      URL: '/img/style/suit.jpg'
    }
  ];

  enums.HATE_IMAGES = [
    {
      CODE: 0,
      URL: '/img/nothing.jpg'
    },
    {
      CODE: 1,
      URL: '/img/hate/check.jpg',
      TEXT: '체크무늬'
    },
    {
      CODE: 2,
      URL: '/img/hate/lettering.jpg',
      TEXT: '레터링'
    },
    {
      CODE: 3,
      URL: '/img/hate/skinnyjean.jpg',
      TEXT: '스키니진'
    },
    {
      CODE: 4,
      URL: '/img/hate/stripe.jpg',
      TEXT: '스트라이프'
    }
  ];

  enums.HATE_IMAGES_NAMES = {
    '0': '없음',
    '1': '체크무늬',
    '2': '레터링',
    '3': '스키니진',
    '4': '스트라이프'
  };


  enums.HEIGHT = {
    MIN: 165,
    MAX: 190
  };

  enums.WEIGHT = {
    MIN: 50,
    MAX: 100
  };

  enums.TOP_SIZE = {
    S: 90,
    M: 95,
    L: 100,
    XL: 105,
    XXL: 110
  };

  enums.BOTTOM_SIZE = {
    MIN: 27,
    MAX: 36
  };

  enums.SHOES_SIZE = {
    MIN: 250,
    MAX: 290
  };

  enums.SURVEY = {
    SEARCH: 0,
    AD: 1,
    BLOG: 2,
    FACEBOOK: 3,
    RECOMMEND: 4,
    ETC: 5
  };

  enums.CONSULT_HOUR = {
    MIN: 10,
    MAX: 20
  };


  enums.PRODUCT = {
    TOP: {
      SHIRT: {
        NAME: '셔츠',
        CODE: 10
      },
      TSHIRT: {
        NAME: '티셔츠',
        CODE: 11
      },
      KNIT: {
        NAME: '니트',
        CODE: 12
      }
    },
    BOTTOM: {
      DENIM: {
        NAME: '청바지',
        CODE: 20
      },
      COTTEN: {
        NAME: '면바지',
        CODE: 21
      }
    },
    OUTER: {
      COAT: {
        NAME: '코트',
        CODE: 30
      },
      JACKET: {
        NAME: '재킷',
        CODE: 31
      },
      SWEATER: {
        NAME: '스웨터',
        CODE: 32
      }
    }
  };

  enums.PRODUCT_NAMES = {
    '10': '셔츠',
    '11': '티셔츠',
    '12': '니트',
    '20': '청바지',
    '21': '면바지',
    '30': '코트',
    '31': '재킷',
    '32': '스웨터'
  };

  enums.AGREE = {
    TERMS: 1,
    PERSONAL: 2
  };

  enums.LIMIT = {
    NAME: 5,  //이름
    EMAIL: 100,  //이메일
    PHONE: 11,  //핸드폰
    ADDRESS: 100,  //주소
    PRODUCT: 4  //상품
  };

  enums.HTTP_STATUS = {
    OK: {
      CODE: 200,
      MESSAGE: 'Success!'
    },
//    NOT_MODIFIED: {
//      CODE: 304,
//      MESSAGE: ''
//    },
    UNAUTHORIZED: {
      CODE: 401,
      MESSAGE: 'Could not authenticate you'
    },
    FORBBIDEN: {
      CODE: 403,
      MESSAGE: 'Your account is suspended and is not permitted to access this feature!'
    },
    NOT_FOUND: {
      CODE: 404,
      MESSAGE: 'Sorry, that page does not exist'
    },
    INTERNAL_SERVER_ERROR: {
      CODE: 500,
      MESSAGE: 'Internal error'
    }
  };

  enums.MSG_VALIDATION = {
    NOT_EMPTY: 'notEmpty() - 파라미터가 비어있습니다.',
    EQUALS: 'equals() - 상수 정의의 값과 같지 않습니다.',
    IS_IN: 'isIn() - 상수 정의에 없는 값입니다.',
    IS_INT: 'isInt() - 정수가 아닙니다.',
    IS_RANGED: 'isRanged() - 범위에 없습니다',
    IS_PHONE_NUMBER: 'isPhoneNumber() - 핸드폰 번호가 아닙니다.',
    IS_POST_CODE: 'isPostCode() - 우편번호 형식이 아닙니다.',
    LEN: 'len() - 파라미터의 길이가 깁니다.',
    IS_POSITIVE_NUMBER: 'isPositiveNumber() - 양의 정수가 아닙니다.',
    IS_IMAGE: 'isImage() - 첨부파일이 이미지가 아닙니다.',
    IS_URL: 'isURL() - URL 형식에 맞지 않습니다.',
    IS_EMAIL: 'isEmail() - Email 형식에 맞지 않습니다.',
    IS_DATE: 'isDate() - 날짜 형식에 맞지 않습니다.',
    IS_ALPHANUMERIC: 'isAlphanumeric() - 영어, 숫자외의 값이 포함되어 있습니다.',
    IS_ALPHAKORNUMERIC: 'isAlphaKorNumeric() - 영어, 한글, 숫자외의 값이 포함되어 있습니다.',
    IS_ARRAY: 'isArray() - 배열이 아닙니다.',
    IS_ARRAY_OR_INTEGER: 'isArrayOrInteger() - 배열이 또는 정수가 아닙니다.',
    IS_ARRAY_CONTAINS: 'isArrayContains() - 배열에 정의에 없는 값이 있습니다.',
    IS_ARRAY_LENGTH: 'isArrayLength() - 배열에 길이가 범위에서 벗어났습니다.'
  };

  (function setNames() {
    enums.TOP_SIZE_KEYS = reverseKeyAndValue(enums.TOP_SIZE);
    enums.LEVEL_NAMES = reverseKeyAndValue(enums.LEVEL);
  })();
  return enums;
})();